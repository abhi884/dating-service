import React from "react";
import { Component } from "react";
import axios from 'axios';

import ProfileList from "../components/profile-list";
import UserPreference from "../components/user-preference";

export default class App extends Component {

	constructor(props) {
		super(props);

		this.state = {
			profilesData: [],
			profiles: []
		};

		this.getProfiles = this.getProfiles.bind(this);
	}

	componentDidMount() {
		axios.get(`https://randomuser.me/api/?results=500&nat=US`)
		    .then(res => {
		        const profilesData = res.data.results.map(profile => {
		        	var ageDifMs = Date.now() - new Date(profile.dob).getTime();
				    var ageDate = new Date(ageDifMs);
				    profile.age = Math.abs(ageDate.getUTCFullYear() - 1970);
				    return profile;
		        });
		        this.setState({ profilesData: profilesData, profiles: profilesData });
		});
	}

	getProfiles(filters) {
		// it should be api call to get data with filters
		// but randomuser api doesn't provide any search api or accepts any params except gender
		// so getting 500 users and filtering on client
		const pData = this.state.profilesData.filter(profile => {
			if ((profile.age >= filters.minAge) &&
				(profile.age <= filters.maxAge) &&
				(filters.gender === 'any' || (profile.gender === filters.gender)) && 
				((filters.search !== '') && 
				 (profile.email.indexOf(filters.search) !== -1) ||
				 (profile.phone.indexOf(filters.search) !== -1) || 
				 (profile.cell.indexOf(filters.search) !== -1))) {
				return true;
			} else {
				return false;
			}
		});

		this.setState({profiles: pData})
	}
		

  render() {
    return (
      	<div>
         	<UserPreference onFilterChange={this.getProfiles} />
			<ProfileList profiles={this.state.profiles} />
      	</div>
    );
  }
}
