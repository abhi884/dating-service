import React, { Component } from 'react';

const ProfileList = props => {
	let pCount = 0;
	const pItems = props.profiles.map(profile => {
		if (pCount > 9) {
			return '';
		}
		pCount++;

		return (
			<li key={profile.id.value} className="profile-panel">
				<img className="photo" src={profile.picture.large}/>
				<span className="name lead">{profile.name.first} {profile.name.last}</span>
				<span className="age lead">{profile.age}yrs</span>
				<button>Contact</button>
			</li>
		);
	});


	return (
		<div id="searchResults">
		<h2>{pCount} Candidates Found</h2>
		<ul>
			{pItems}
		</ul>
		</div>
	);
}

export default ProfileList;
