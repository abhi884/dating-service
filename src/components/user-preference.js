import React, { Component } from 'react';

class UserPreference extends Component {
	constructor(props) {
		super(props);

		this.state = {
			minAge: 18,
			maxAge: 90,
			gender: 'any',
			search: ''
		};

		this.submit = this.submit.bind(this);
		this.reset = this.reset.bind(this);
	}

	submit(e) {
		this.props.onFilterChange({
			minAge: this.state.minAge,
			maxAge: this.state.maxAge,
			gender: this.state.gender,
			search: this.state.search.trim()
		});
		if (e) {
			e.preventDefault();
		}
	}

	reset() {
		this.setState({
			minAge: 18,
			maxAge: 90,
			gender: 'any',
			search: ''
		}, this.submit)
	}

	render() {
		return (
			<div id="userPreferences">
		        <h2>Search Criteria</h2>
		        <form className="form">
		          <div className="form-group">
		            <label className="control-label">Age</label>
		            <input type="text" name="ageMin" 
		            	value={this.state.minAge}
		            	onChange={event => this.setState({minAge:  event.target.value})}/> to
		            <input type="text" name="ageMax" 
		            	value={this.state.maxAge}
		            	onChange={event => this.setState({maxAge:  event.target.value})}/>
		          </div>

		          <div className="form-group">
		            <label className="control-label">Gender</label>
		            <label className="radio-inline"><input type="radio" name="gender" value="any" 
		            	checked={this.state.gender === 'any'} 
		            	onChange={event => this.setState({gender:  event.target.value})}/>Any</label>
		            <label className="radio-inline"><input type="radio" name="gender" value="male"
		            	checked={this.state.gender === 'male'}
		            	onChange={event => this.setState({gender:  event.target.value})}/>Male</label>
		            <label className="radio-inline"><input type="radio" name="gender" value="female"
		            	checked={this.state.gender === 'female'}
		            	onChange={event => this.setState({gender:  event.target.value})}/>Female</label>
		          </div>

		          <div className="form-group">
		            <label className="control-label">Search</label>
		            <input type="text" name="search" className="search-box"
		            	value={this.state.search}
		            	onChange={event => this.setState({search:  event.target.value.trim()})}
		            	placeholder="Search Email, Phone, Cell"/>
		          </div>

		          <button type="button" className="btn btn-default" onClick={this.reset}>Reset</button>
		          <button type="submit" className="btn btn-primary" onClick={this.submit}>Filter</button>
		        </form>
		    </div>
		);
	}
}

export default UserPreference;
